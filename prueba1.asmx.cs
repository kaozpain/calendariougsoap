﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.Net;
using System.Net.Mail;
using System.Web.Script.Serialization;
//using System.Data.OracleClient;
using Oracle.ManagedDataAccess.Client;
//using Oracle.DataAccess.Client;
//using Oracle.DataAccess.Types;
using System.Runtime.Serialization.Json;
using System.IO;
using System.Text;

namespace CalendarSoap
{
    /// <summary>
    /// Summary description for prueba1
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class prueba1 : System.Web.Services.WebService
    {
        public static string oradb = "Data Source=localhost:1521;User Id=adrian;Password=adrian;";
        OracleConnection conn = new OracleConnection(oradb);
        OracleCommand cmd = new OracleCommand();

        [WebMethod]
        public string HelloWorld()
        {
            return "Hello World";
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Autenticacion(string usuario, string clave)
        {
            //string 33, error al conectar con bd, si devuelve 10 no existe o credenciales mal puestas
            //codigo 8 acceso correcto
            Usuario aux = new Usuario();
            aux.codigo = 10;
            try {
                cmd.Connection = conn;
                cmd.CommandText = "select loge.cedula, us.correo, us.nombre, us.tipo from ug_login loge, ug_usuarios us where loge.usuario='" + usuario + "' and loge.clave='" + clave + "' and us.cedula=loge.cedula";
                conn.Open();
                OracleDataReader dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    aux.nombre = dr["nombre"].ToString();
                    aux.cedula = dr["cedula"].ToString();
                    aux.tipo = Convert.ToInt32(dr["tipo"]);
                    aux.correo = dr["correo"].ToString();
                    aux.codigo = 8;
                }
                conn.Close();
            }
            catch (Exception)
            {
                aux.codigo = 33;
            }
            string devolver = new JavaScriptSerializer().Serialize(aux);
            return devolver;
        }

        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public int ExisteCliente(string usuario, string correo)
        {
            int cod = 8;
            //codigo 33 error al conectar con base de datos, codigo 22 cliente ya registrado
            //codigo 10 no existe correo, codigo 12 cuenta ya usada, codigo 8 acceso correcto
            try {
                cmd.Connection = conn;
                cmd.CommandText = "select us.* from ug_login loge, ug_usuarios us where us.correo='" + correo + "' and us.cedula=loge.cedula";
                conn.Open();
                OracleDataReader dr = cmd.ExecuteReader();
                if (dr.HasRows) cod = 22;
                cmd.CommandText = "select usuario from ug_login where usuario='" + usuario + "'";
                dr = cmd.ExecuteReader();
                if (dr.HasRows) cod = 12;
                cmd.CommandText = "select correo from ug_usuarios where correo= '" + correo + "'";
                dr = cmd.ExecuteReader();
                if (!dr.HasRows) cod = 10;
                conn.Close();
            }
            catch (Exception)
            {
                cod = 33;
            }
            return cod;
        }



        [WebMethod]
        //[ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public int EnviarOTP(string usuario, string correo)
        {
            //codigo 120 envio correcto, codigo 99 error del correo,
            //codigo 33 error con base de datos
            int cod = 120;
            MailMessage mail = new MailMessage();
            SmtpClient cliente = new SmtpClient();
            Random rand = new Random();
            string posibles = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            int longitud = posibles.Length;
            char letra;
            int longitudnuevacadena = 6;
            string nuevacadena = "";
            for (int i = 0; i < longitudnuevacadena; i++)
            {
                letra = posibles[rand.Next(longitud)];
                nuevacadena += letra.ToString();
            }
            try
            {
                cmd.Connection = conn;
                cmd.CommandText = "merge into ug_otp USING(SELECT sysdate FROM DUAL)" +
                          " ON(correo = '" + correo + "') WHEN MATCHED THEN UPDATE " +
                          "SET codigo = '" + nuevacadena + "' WHEN NOT MATCHED THEN " +
                          "insert(codigo, correo) values('" + nuevacadena + "', '" + correo + "')";
                conn.Open();
                cmd.ExecuteNonQueryAsync();
                conn.Close();
            } catch (Exception)
            {
                return cod = 33;
            }
            try {
                mail.To.Clear();
                mail.Body = "CalendarioUG le saluda, su codigo de validacion es el <h3>" + nuevacadena + "</h3>";
                mail.Subject = "Codigo CalenadrioUG";
                mail.IsBodyHtml = true;
                mail.To.Add(correo.Trim());
                mail.From = new MailAddress("adrian272566@gmail.com");
                cliente.Credentials = new NetworkCredential("adrian272566@gmail.com", "d4v0l0r3nz0");
                cliente.Host = "smtp.gmail.com";
                cliente.Port = 587;
                cliente.EnableSsl = true;
                cliente.Send(mail);
            }
            catch (Exception)
            {
                cod = 99;
            }
            return cod;
        }

        [WebMethod]
        public int ValidarOTP(string codigo, string correo)
        {
            //codigo 8 codigo correcto, codigo 12 codigo incorrecto
            //codigo 33 error en conexion con bd
            int cod = 8;
            try {
                cmd.Connection = conn;
                cmd.CommandText = "select * from ug_otp where codigo='" + codigo + "' and correo= '" + correo + "'";
                conn.Open();
                OracleDataReader dr = cmd.ExecuteReader();
                if (!dr.HasRows) cod = 12;
                conn.Close();
            }
            catch (Exception)
            {
                cod = 33;
            }
            return cod;
        }

        [WebMethod]
        public int registro(string usuario, string clave, string correo)
        {
            //codigo 8 registro exitoso, codigo 33 error al conectar con bd
            int cod = 8;
            try {
                cmd.Connection = conn;
                conn.Open();
                cmd.CommandText = "insert into ug_login (usuario, clave, cedula) select '" + usuario + "','" + clave + "',cedula from ug_usuarios where correo='" + correo + "'";
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                cod = 33;
            }
            return cod;
        }

        [WebMethod]
        public int AgregarEvento(int id_universidad, int id_campus, int id_facultad, int id_carrera,
            int id_materia, string fecha_inicio, string fecha_fin, int clases, string titulo, string descripcion)
        {
            //codigo 8 registro exitoso, codigo 33 error al conectar con bd
            int cod = 8;
            try
            {
                cmd.Connection = conn;
                conn.Open();
                cmd.CommandText = "insert into ug_eventos (id_universidad,id_campus,id_facultad,id_carrera,id_materia,fecha_inicio,fecha_fin,clases,titulo,descripcion) values (" + id_universidad + "," + id_campus + "," + id_facultad + "," + id_carrera + "," + id_materia + ",'" + Convert.ToDateTime(fecha_inicio).ToString("dd/MM/yyyy") + "','" + Convert.ToDateTime(fecha_fin).ToString("dd/MM/yyyy") + "'," + clases + ",'" + titulo + "','" + descripcion + "')";
                cmd.ExecuteNonQuery();
                conn.Close();
            }
            catch (Exception)
            {
                cod = 33;
            }
            return cod;
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodosLosEventos(int id)
        {
            List<Evento> Lista = new List<Evento>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_eventos where id_evento>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Evento() {
                    id_evento = Convert.ToInt32(dr["id_evento"]),
                    nombre = dr["titulo"].ToString(),
                    descripcion = dr["descripcion"].ToString(),
                    clases = Convert.ToInt32(dr["clases"]),
                    fechainicio = Convert.ToDateTime(dr["fecha_inicio"]).ToString("dd/MM/yy"),
                    fechafin = Convert.ToDateTime(dr["fecha_fin"]).ToString("dd/MM/yy"),
                    id_universidad = Convert.ToInt32(dr["id_universidad"]),
                    id_campus = Convert.ToInt32(dr["id_campus"]),
                    id_facultad = Convert.ToInt32(dr["id_facultad"]),
                    id_carrera = Convert.ToInt32(dr["id_carrera"]),
                    id_materia = Convert.ToInt32(dr["id_materia"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodosLosFeriados(int id)
        {
            List<Feriado> Lista = new List<Feriado>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_feriados where id_feriado>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Feriado()
                {
                    id_feriado = Convert.ToInt32(dr["id_feriado"]),
                    id_ciudad = Convert.ToInt32(dr["id_ciudad"]),
                    nombre = dr["titulo"].ToString(),
                    descripcion = dr["descripcion"].ToString(),
                    id_pais = Convert.ToInt32(dr["id_pais"]),
                    clases = Convert.ToInt32(dr["clases"]),
                    fechainicio = Convert.ToDateTime(dr["fecha_inicio"]).ToString("dd/MM/yy"),
                    fechafin = Convert.ToDateTime(dr["fecha_fin"]).ToString("dd/MM/yy"),
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodosLosCampus(int id)
        {
            List<Campus> Lista = new List<Campus>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_campus where id_campus>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Campus()
                {
                    id_campus = Convert.ToInt32(dr["id_campus"]),
                    nombre = dr["nombre_campus"].ToString(),
                    id_universidad = Convert.ToInt32(dr["id_universidad"]),
                    encargado=Convert.ToInt32(dr["encargado"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodasLasCarreras(int id)
        {
            List<Carrera> Lista = new List<Carrera>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_carrera where id_carrera>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Carrera()
                {
                    id_carrera = Convert.ToInt32(dr["id_carrera"]),
                    nombre = dr["nombre_carrera"].ToString(),
                    id_facultad = Convert.ToInt32(dr["id_facultad"]),
                    encargado = Convert.ToInt32(dr["encargado"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodasLasCiudades(int id)
        {
            List<Ciudad> Lista = new List<Ciudad>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_ciudad where id_ciudad>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Ciudad()
                {
                    id_ciudad = Convert.ToInt32(dr["id_ciudad"]),
                    nombre = dr["nombre"].ToString(),
                    id_pais = Convert.ToInt32(dr["id_pais"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodosLosCursos(int id)
        {
            List<Curso> Lista = new List<Curso>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_curso where id_curso>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Curso()
                {
                    id_curso = Convert.ToInt32(dr["id_curso"]),
                    nombre = dr["nombre"].ToString(),
                    modalidad = dr["modalidad"].ToString()
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodasLasFacultades(int id)
        {
            List<Facultad> Lista = new List<Facultad>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_facultad where id_facultad>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Facultad()
                {
                    id_facultad = Convert.ToInt32(dr["id_facultad"]),
                    nombre = dr["nombre_facultad"].ToString(),
                    id_campus = Convert.ToInt32(dr["id_campus"]),
                    encargado = Convert.ToInt32(dr["encargado"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodasLasMaterias(int id)
        {
            List<Materia> Lista = new List<Materia>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_materia where id_materia>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Materia()
                {
                    id_materia = Convert.ToInt32(dr["id_materia"]),
                    nombre = dr["nombre"].ToString(),
                    cedula = Convert.ToInt32(dr["cedula"]),
                    id_curso = Convert.ToInt32(dr["id_curso"]),
                    semestre = Convert.ToInt32(dr["semestre"]),
                    id_carrera = Convert.ToInt32(dr["id_carrera"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodosLosPaises(int id)
        {
            List<Pais> Lista = new List<Pais>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_pais where id_pais>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Pais()
                {
                    id_pais = Convert.ToInt32(dr["id_pais"]),
                    nombre = dr["nombre"].ToString(),
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodasLasUbicaciones(int id)
        {
            List<Ubicacion> Lista = new List<Ubicacion>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_ubicacion_facultad where id_ubicacion>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Ubicacion()
                {
                    id_ubicacion = Convert.ToInt32(dr["id_ubicacion"]),
                    longitud = dr["longitud"].ToString(),
                    latitud = dr["latitud"].ToString(),
                    id_facultad = Convert.ToInt32(dr["id_facultad"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String TodasLasUniversidades(int id)
        {
            List<Universidad> Lista = new List<Universidad>();
            cmd.Connection = conn;
            cmd.CommandText = "select * from ug_universidad where id_universidad>" + id;
            conn.Open();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                Lista.Add(new Universidad()
                {
                    id_universidad = Convert.ToInt32(dr["id_universidad"]),
                    nombre = dr["nombre_universidad"].ToString(),
                    id_ciudad = Convert.ToInt32(dr["id_ciudad"]),
                    encargado = Convert.ToInt32(dr["encargado"])
                });
            }
            conn.Close();
            return new JavaScriptSerializer().Serialize(Lista);
        }


        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public String MatriculaVigente(int cedula, int id)
        {
            String devolver;
            DateTime hoy = DateTime.Today;
            cmd.Connection = conn;
            String hoyText = hoy.ToString("dd/MM/yyyy");
            cmd.CommandText = "select * from ug_matricula where cedula=" + cedula + " and fecha_exp=(select max(fecha_exp) from ug_matricula where cedula= " + cedula + ") and fecha_exp > TO_DATE('"+hoyText+"','dd/MM/yyyy') and id_matricula!="+id;
            conn.Open();
            Matricula matricula = new Matricula();
            OracleDataReader dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                matricula.id_matricula = Convert.ToInt32(dr["id_matricula"]);
                matricula.cedula = Convert.ToInt32(dr["cedula"]);
                matricula.id_carrera = Convert.ToInt32(dr["id_carrera"]);
                matricula.fecha = Convert.ToDateTime(dr["fecha"]).ToString("dd/MM/yy");
                matricula.fecha_exp = Convert.ToDateTime(dr["fecha_exp"]).ToString("dd/MM/yy");
                matricula.id_materia1 = Convert.ToInt32(dr["id_materia1"]);
                matricula.id_materia2 = Convert.ToInt32(dr["id_materia2"]);
                matricula.id_materia3 = Convert.ToInt32(dr["id_materia3"]);
                matricula.id_materia4 = Convert.ToInt32(dr["id_materia4"]);
                matricula.id_materia5 = Convert.ToInt32(dr["id_materia5"]);
                matricula.id_materia6 = Convert.ToInt32(dr["id_materia6"]);
                matricula.id_materia7 = Convert.ToInt32(dr["id_materia7"]);
                matricula.id_materia8 = Convert.ToInt32(dr["id_materia8"]);
                matricula.id_materia9 = Convert.ToInt32(dr["id_materia9"]);
            }
            conn.Close();
            devolver = new JavaScriptSerializer().Serialize(matricula);
            if (matricula.id_matricula == 0) devolver = "10";
            return devolver;
        }
    }
}