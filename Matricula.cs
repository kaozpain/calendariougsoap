﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Matricula
    {
        public int id_matricula { get; set; }
        public int cedula { get; set; }
        public int id_carrera { get; set; }
        public String fecha { get; set; }
        public String fecha_exp { get; set; }
        public int id_materia1 { get; set; }
        public int id_materia2 { get; set; }
        public int id_materia3 { get; set; }
        public int id_materia4 { get; set; }
        public int id_materia5 { get; set; }
        public int id_materia6 { get; set; }
        public int id_materia7 { get; set; }
        public int id_materia8 { get; set; }
        public int id_materia9 { get; set; }
    }
}