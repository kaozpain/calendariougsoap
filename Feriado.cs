﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Feriado
    {
        public int id_feriado { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int clases { get; set; }
        public string fechainicio { get; set; }
        public string fechafin { get; set; }
        public int id_pais { get; set; }
        public int id_ciudad { get; set; }
    }
}