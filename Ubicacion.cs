﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Ubicacion
    {
        public string longitud { get; set; }
        public int id_ubicacion { get; set; }
        public string latitud { get; set; }
        public int id_facultad { get; set; }
    }
}