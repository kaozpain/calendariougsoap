﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Ciudad
    {
        public string nombre { get; set; }
        public int id_ciudad { get; set; }
        public int id_pais { get; set; }
    }
}