﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Evento
    {
        public int id_evento { get; set; }
        public string nombre { get; set; }
        public string descripcion { get; set; }
        public int clases { get; set; }
        public string fechainicio{ get; set; }
        public string fechafin { get; set; }
        //public DateTime fechainicio { get; set; }
        //public DateTime fechafin { get; set; }
        public int id_facultad { get; set; }
        public int id_campus { get; set; }
        public int id_universidad { get; set; }
        public int id_materia { get; set; }
        public int id_carrera { get; set; }

        public Evento()
        {
        }
    }
}