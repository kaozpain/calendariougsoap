﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Curso
    {
        public string nombre { get; set; }
        public int id_curso { get; set; }
        public string modalidad { get; set; }
    }
}