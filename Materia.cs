﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Materia
    {
        public string nombre { get; set; }
        public int id_materia { get; set; }
        public int cedula { get; set; }
        public int id_curso { get; set; }
        public int semestre { get; set; }
        public int id_carrera { get; set; }
    }
}