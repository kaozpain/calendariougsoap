﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Universidad
    {
        public string nombre { get; set; }
        public int id_universidad { get; set; }
        public int id_ciudad { get; set; }
        public int encargado { get; set; }
    }
}