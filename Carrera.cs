﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CalendarSoap
{
    public class Carrera
    {
        public string nombre { get; set; }
        public int id_carrera { get; set; }
        public int id_facultad { get; set; }
        public int encargado { get; set; }
    }
}